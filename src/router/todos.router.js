const todosRouter = [
  {
    path: "/todos",
    name: "todos",
    component: () => import("@/views/Todos/Index.vue"),
  },
  {
    path: "/todos/:id",
    name: "todo",
    component: () => import("@/views/Todos/Todo.vue"),
  },
];

export default todosRouter;
