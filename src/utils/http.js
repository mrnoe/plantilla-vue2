import axios from "axios";

console.log(process.env.VUE_APP_BACKEND_URL);

const instance = axios.create({
  baseURL: process.env.VUE_APP_BACKEND_URL || "localhost:8000",
  timeout: 5000,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
});

console.log(instance.getUri());

const http = async (url = "", params = {}, method = "GET", headers = {}) => {
  switch (method.toLowerCase()) {
    case "get":
      return await instance.get(url, { params, headers });

    case "post":
      return await instance.post(url, params, { headers });

    case "put":
      return await instance.get(url, params, { headers });

    case "delete":
      return await instance.delete(url, { params, headers });

    default:
      break;
  }
};

export default http;
