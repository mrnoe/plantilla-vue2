import http from "@/utils/http";

const getTodos = async (params = {}) => await http("todos", params, "GET");
const getTodo = async (id, params = {}) =>
  await http(`todos/${id}`, params, "GET");

export default {
  getTodos,
  getTodo,
};
